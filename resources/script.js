const headers = new Headers();
// note, userName and streamKey are both derived in a file called data.js
headers.append('Authorization', 'Basic ' + btoa(userName + ":" + streamKey));

async function getEndpoint(url = '') {
  const response = await fetch(url, {method:'GET', headers: headers})
  const data = await response.json()
  return data
 }

 async function postEndpoint(url = '', data = {}) {
    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(data), // serialized data
      headers: headers  // authentication header
    })
    return response
 }

 async function getStatus() {
  return getEndpoint(api_url + 'admin/status')
 }

 async function updateElements(data) {
  var {online, viewerCount, sessionPeakViewerCount} = data
  updateViewers(viewerCount);
  updateOnline(online);
  updateSessionPeak(sessionPeakViewerCount);

}

async function updateOnline(online) {
  document.getElementById('online').innerHTML = online
}
async function updateViewers(viewers) {
  document.getElementById('currentViewers').innerHTML = viewers
}
async function updateSessionPeak(viewers) {
  document.getElementById('sessionPeak').innerHTML = viewers
}

async function Main() {
  // update visual elements using data from 'api/admin/status'
  // updateElements(await getStatus())
  const response = await fetch(api_url + 'yp', {mode:'no-cors'})
  const data = await response.json()

  // update the broadcast title
  // postEndpoint(``
    // api_url + 'admin/config/streamtitle',
    // {value: "testing" + Math.floor(Math.random() * 1000)}
  // ).then(response => response.status)
  // .then(data => console.log(data))
}

// Main()
