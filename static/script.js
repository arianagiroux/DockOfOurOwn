async function getEndpoint(url = '') {
	const response = await fetch(url, {method:'GET'});
	const data = await response.json();
	return data;
}

async function uiUpdateOnce() {
	var data = await getEndpoint('http://127.0.0.1:5000/api/serverstatus');
	document.getElementById("streamTitle").innerHTML = data.streamTitle;
	document.getElementById("currentViewers").innerHTML = data.viewerCount;
	document.getElementById("sessionMaxViewerCount").innerHTML = data.sessionMaxViewerCount;
	document.getElementById("overallMaxViewerCount").innerHTML = data.overallMaxViewerCount;
	document.getElementById("tags").innerHTML = data.tags;
}

async function continuousUiUpdate() {
	window.setInterval(async () => {
		uiUpdateOnce();
	}, 1000);
}

async function updateStreamTitle(value) {
	// this function simply passes data along to the python script, and allows
	// python to handle data processing
	response = await fetch('http://127.0.0.1:5000/api/update/streamtitle',
		{
			method:'POST',
			body:JSON.stringify(value)
		}
	);
	await uiUpdateOnce();
}

async function updateTags(value) {
	// this function simply passes data along to the python script, and allows
	// python to handle data processing
	response = await fetch('http://127.0.0.1:5000/api/update/servertags',
		{
			method:'POST',
			body:JSON.stringify(value)
		}
	);
	await uiUpdateOnce();
}

continuousUiUpdate()
